vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()

-- Packer (менеджер плагинов)
use 'wbthomason/packer.nvim'




-- ОФОРМЛЕНИЕ {{{1

-- Цветовые схемы {{{2
use 'joshdick/onedark.vim' -- onedark
use 'damage220/solas.vim' -- solas
use 'nanotech/jellybeans.vim' -- jellybeans
use 'duythinht/vim-coffee' -- coffee
use 'morhetz/gruvbox' -- gruvbox
use 'tomasiser/vim-code-dark' -- codedark
-- }}}

-- Lualine (строка состояния)
use { 'nvim-lualine/lualine.nvim',
requires = {'kyazdani42/nvim-web-devicons', opt = true},
config = function()
require('lualine').setup()
end, }




-- ИНТЕРФЕЙС {{{1

-- Вкладки
use {'akinsho/bufferline.nvim', requires = 'kyazdani42/nvim-web-devicons',
config = function()
require("bufferline").setup{}
end, }

-- Файловый менеджер
use 'francoiscabrol/ranger.vim'

-- Буфер
use 'sandeepcr529/buffet.vim'

-- Закрывашка
use 'rbgrouleff/bclose.vim'

--- Popup окошки
use 'nvim-lua/popup.nvim'

-- Стартовая страница, если просто набрать vim в консоле
use 'mhinz/vim-startify'




-- ПОДДЕРЖКА ЯЗЫКОВ {{{1

-- Python
use 'fisadev/vim-isort' -- шапка с импортами приводим в порядок
use 'mitsuhiko/vim-jinja' -- поддержка темплейтом jinja2

-- HTML
-- Подсвечивает закрывающий и откры. тэг. Если, где-то что-то не закрыто, то не подсвечивает.
use 'idanarye/breeze.vim'
-- Закрывает автоматом  html и xml тэги. Пишешь <h1> и он автоматом закроется </h1>
use 'alvan/vim-closetag'

-- CSS
use 'ap/vim-css-color' -- Подсвечивает #ffffff




-- НАПИСАНИЕ ТЕКСТА {{{1

-- Автоформатирование кода для всех языков
use 'Chiel92/vim-autoformat'

-- ]p - вставить на строку выше, [p - ниже
use 'tpope/vim-unimpaired'

-- Обрамляет или снимает обрамление. Выдели слово, нажми S и набери <h1>
use 'tpope/vim-surround'

-- Комментирует по gc все, вне зависимости от языка программирования
use { 'numToStr/Comment.nvim',
config = function() require('Comment').setup() end }

-- Обрамляет строку в теги по ctrl- y + ,
use 'mattn/emmet-vim'

-- Закрывает автоматом скобки
use 'cohama/lexima.vim'

-- Может повторять через . vimsurround
use 'tpope/vim-repeat'

-- Проверка синтаксиса, все языки
use 'dense-analysis/ale'




-- СЛОВАРИ {{{1

-- Даже если включена русская раскладка vim команды будут работать
use 'powerman/vim-plugin-ruscmd'

-- Переводчик рус - англ
use 'skanehira/translate.vim'





-- АВТОКОМПЛИТ {{{1

--- lsp-zero
use {
'VonHeikemen/lsp-zero.nvim',
requires = {
-- LSP Support
{'neovim/nvim-lspconfig'},
{'williamboman/nvim-lsp-installer'},

-- Autocompletion
{'hrsh7th/nvim-cmp'},
{'hrsh7th/cmp-buffer'},
{'hrsh7th/cmp-path'},
{'saadparwaiz1/cmp_luasnip'},
{'hrsh7th/cmp-nvim-lsp'},
{'hrsh7th/cmp-nvim-lua'},

-- Snippets
{'L3MON4D3/LuaSnip'},
{'rafamadriz/friendly-snippets'},
}
}


































end)