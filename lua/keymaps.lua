local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }
local g = vim.g                 -- global variables
g.mapleader = ' '




-- РЕМАП {{{1


-- Всякое {{{2

map('', '<F12>', '<F1>', opts) -- справку куда-нибудь подальше


-- Файл конфигурации {{{2

-- <F3> перечитать конфигурацию nvim Может не работать, если echo $TERM  xterm-256color
map('n', '<F3>', ':so ~/.config/nvim/init.lua<CR>:so ~/.config/nvim/lua/plugins.lua<CR>:so ~/.config/nvim/lua/settings.lua<CR>:so ~/.config/nvim/lua/keymaps.lua<CR>', { noremap = true })

-- <S-F3> Открыть всю nvim конфигурацию для редактирования
map('n', '<S-F3>', ':e ~/.config/nvim/init.lua<CR>:e ~/.config/nvim/lua/plugins.lua<CR>:e ~/.config/nvim/lua/settings.lua<CR>:e ~/.config/nvim/lua/keymaps.lua<CR>', { noremap = true })


-- Режимы {{{2

map('i', 'jj', '<Esc>', opts) -- возврат в нормальный режим из режима вставки
map('v', 'jj', '<Esc>', opts) -- возврат в нормальный режим из режима выделения


-- Оформлене {{{2

map('', '<leader>/', ':noh<CR>', opts) -- выключает подсветку поисковой выдачи

-- <F5> разные вариации нумераций строк, можно переключаться
map('n', '<F5>', ':exec &nu==&rnu? "se nu!" : "se rnu!"<CR>', opts)


-- Перемещение {{{2

map('', "'", ';', opts) -- повтор строчного поиска

map('', 'j', 'h', opts) -- влево
map('', 'k', 'j', opts) -- вниз
map('', 'l', 'k', opts) -- вверх
map('', ';', 'l', opts) -- вправо

map('', '<leader>h', '0', opts) -- к началу строки
map('', '<leader>j', '^', opts) -- к первому не пробельному символу строки
map('', '<leader>;', '$', opts) -- к концу строки

map('', 'K', '}', opts) -- абзац вниз
map('', 'L', '{', opts) -- абзац ввверх

map('', '<leader>m', 'M', opts) -- курсор к центру экрана
map('', '<leader>k', 'L', opts) -- курсор вниз экрана
map('', '<leader>l', 'H', opts) -- курсор вверх экрана

map('i', '<C-k>', '<left>', opts) -- влево (в режиме вставки)
map('i', '<C-l>', '<right>', opts) -- вправо (в режиме вставки)


-- Копипаста {{{2

map('i', '<C-v>', '<Esc>"+pa', opts)
map('v', '<C-c>', '"+y', opts)


-- Сохранение и выход {{{2

map('', '<leader>w', ':w<CR>', opts)
map('', '<leader>x', ':x<CR>', opts)




-- МАПЫ ПЛАГИНОВ {{{1

-- Ranger (файловый менеджер)
map('', '<leader>z', ':Ranger<CR>', opts)

-- Buffet (буфер)
map('', '<leader>a', ':Bufferlist<CR>', opts)

-- Bclose (закрывашка)
map('', '<leader>q', ':Bclose<CR>', opts)

-- Bufferline (вкладки)
map('n', '<Tab>', ':BufferLineCycleNext<CR>', opts) -- вправо
map('n', '<S-Tab>', ':BufferLineCyclePrev<CR>', opts) -- влево