local cmd = vim.cmd -- Vim команды
local exec = vim.api.nvim_exec -- Vim скрипты
local opt = vim.opt -- глобыльные и буфферные параметры в области окна




-- ОФОРМЛЕНИЕ {{{1

opt.cursorline = true -- обозначает строку курсора
opt.cursorcolumn = true -- обозначает колонку курсора
opt.number = true -- включает нумерацию строк
opt.relativenumber = true -- включает относительную нумерацию строк
opt.undofile = true -- возможность отката  назад
opt.termguicolors = true -- цветность  24-bit RGB
cmd'colorscheme gruvbox' -- цветовая схема
opt.so=999 -- строка курсора всегда по центру

-- Подсвечивает на доли секунды скопированную часть текста
exec([[
augroup YankHighlight
autocmd!
autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=700}
augroup end
]], false)

-- cmd([[
-- filetype indent plugin on
-- syntax enable
-- ]])




-- ТАБЫ {{{1

opt.expandtab = true -- в режиме вставки заменяет символ табуляции на соответствующее количество пробело
opt.shiftwidth = 4 -- длинна добавляемого строке таба
opt.tabstop = 4 -- длинна таба
opt.smartindent = true -- добавить таб новой строки




-- СПЛИТ {{{1

opt.splitright = true -- вертикальный сплит
opt.splitbelow = true -- горизонтальный сплит




-- СЛОВАРЬ {{{1

opt.spelllang= { 'en_us', 'ru' } -- словари рус англ




-- НАПИСАНИЕ ТЕКСТА {{{1

cmd [[au BufEnter * set fo-=c fo-=r fo-=o]] -- не комментирую новую строку

-- Запоминает где nvim последний раз редактировал файл
cmd [[
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
]]




-- ПОДДЕРЖКА ЯЗЫКОВ {{{1

-- С этой строкой отлично форматирует html файл, который содержит jinja2
cmd[[ autocmd BufNewFile,BufRead *.html set filetype=htmldjango ]]





















local lsp = require('lsp-zero')

lsp.preset('recommended')
lsp.setup()